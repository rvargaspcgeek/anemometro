<?php
date_default_timezone_set('America/Santiago');
require_once ('conexion.php');
$t = new DateTime( date('G:i:s') );
$t->sub( new DateInterval('PT1H') );
$hora_atras=$t->format('Y-m-d H:i:s');

$centro=$_GET["centro"];
$mi_conexion= new conexion();
$conn= $mi_conexion->conectar();

//$mi_conexion->conectar();
$conn=$mi_conexion->conexion;
$consulta = "select direccion_rosa, count(direccion_rosa)  as contador from ".$centro." where fecha_hora >= '".$hora_atras."' group by direccion_rosa order by contador desc";

// echo $consulta;
$resultado = mysqli_query($conn, $consulta);
$columna = mysqli_fetch_array( $resultado );
//$resultado=$mi_conexion->mysqli_query($consulta);
$consulta2="select  avg(velocidad) as velocidad_promedio from ".$centro."  where fecha_hora >='".$hora_atras."'";
$resultado2 = mysqli_query($conn, $consulta2);
$columna2 = mysqli_fetch_array( $resultado2 );
$nudos_promedio=($columna2["velocidad_promedio"]/1.85);
?>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <span class="label label-primary pull-left">Hoy</span>
        <h5><?php echo  date('d-m-Y G:i:s'); ?></h5>
    </div>
    <div    class="ibox-content"><strong>Dirección</strong> (tendencia):
        <h2 class="no-margins"><?php echo dime_tu_direccion2($columna["direccion_rosa"]); ?></h2>
        <br>
        <small><strong>  Velocidad Promedio: </strong></small>

        <h2 class="no-margins"><?php echo number_format($columna2["velocidad_promedio"],'2'); ?></h2>
        <small class="font-bold text-navy" style="padding-left: 60px">Km/H</small>
        <br><br>
        <h2 class="no-margins"><?php echo number_format($nudos_promedio,'2'); ?></h2>
        <small class="font-bold text-navy" style="padding-left: 60px">Nudos</small>

    </div>
</div>

<?php
function dime_tu_direccion2($direccion){
    if ($direccion==0)
        $dir="NORTE";
    else if ($direccion==1)
        $dir="NORESTE";
    else if ($direccion==2)
        $dir="ESTE";
    else if ($direccion==3)
        $dir="SURESTE";
    else if ($direccion==4)
        $dir="SUR";
    else if ($direccion==5)
        $dir="SUROESTE";
    else if ($direccion==6)
        $dir="OESTE";
    else if ($direccion==7)
        $dir="NOROESTE ";
    else
        $dir= "No hay lecturas de viento";
    return $dir;

}
?>