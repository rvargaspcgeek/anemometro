<?php
session_start();
date_default_timezone_set('America/Santiago');
require_once ('conexion.php');
$t = new DateTime( date('G:i:s') );
$t->sub( new DateInterval('PT1H') );
$hora_atras=$t->format('Y-m-d H:i:s');
if (!isset($_GET['valida']))
$_SESSION["error"]="";
//resource imagegrabscreen ( void );
//$_SESSION["id_perfil"]="";
//$_SESSION["id_usuario"]="";
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Anemometro</title>
    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
   <!-- <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>-->
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
    <link rel="shortcut icon" type="image/png" href="img/sol.png"/>
</head>
<body style="background-image: url('img/portada1.jpg'); background-repeat: no-repeat">

<div class="limiter">
    <div class="container-login100" >
        <div class="wrap-login100">
            <form class="login100-form validate-form" name="form1" id="form1" method="post" action="valida.php">
					<span class="login100-form-logo">
						<img src="img/rosa.png" width="100px"  height="100px" >
					</span>

                <span class="login100-form-title p-b-34 p-t-27">
                    INVERMAR S.A. </BR>
                    Sistema Anemómetro
					</span>
                <div  id="error" name="error" class="text-danger"><?php echo  $_SESSION['error']; ?></div>
                <hr>
                <div class="wrap-input100 validate-input" data-validate = "Ingrese Usuario">
                    <input class="input100" type="text" name="usr" id="usr" placeholder="Usuario" >
                    <span class="focus-input100" data-placeholder="&#xf207;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Ingrese Clave">
                    <input class="input100" type="password" name="pwd" id="pwd" placeholder="Clave" value="">
                    <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>

                <div class="contact100-form-checkbox">
                    <input class="input-checkbox100" id="ckb1" type="checkbox" checked name="remember-me">
                    <label class="label-checkbox100" for="ckb1">
                        Recordar Cuenta
                    </label>
                </div>

                <button class="container-login100-form-btn">
                    <input type="submit" class="login100-form text-danger"   value="Entrar">
                </div>
                <!--
                <div class="text-center p-t-90">
                    <a class="txt1" href="#">
                        Forgot Password?
                    </a>
                </div>-->
            </form>
        </div>
    </div>
</div>


<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>
<script>
    function valida_usuario(){
        $("#error").html("");
        // alert('entró');
        console.log('entro ajax')
            $.ajax({

                type: "POST",
                url: 'valida.php',
                data: $('#form1').serialize(),
                dataType: 'json',
            success: function (data) {

                console.log('holaa')
                console.log(data)
                if(data.error === "ok")
                {
                    //alert('okok');
                window.location.href="main.php";
                }
                else{
                    $("#error").html(data.error);
                }



            },
            error: function (data) {
                console.log("error" + data)

            }
        })
    }
</script>

</body>

</html>