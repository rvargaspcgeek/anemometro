<?php
date_default_timezone_set('America/Santiago');
require_once ('conexion.php');
$t = new DateTime( date('G:i:s') );
$t->sub( new DateInterval('PT1H') );
$hora_atras=$t->format('Y-m-d H:i:s');

$centro=$_GET["centro"];
$mi_conexion= new conexion();
$conn= $mi_conexion->conectar();


//$mi_conexion->conectar();
$conn=$mi_conexion->conexion;
$consulta = "SELECT * FROM ".$centro." where fecha_hora >= '".$hora_atras."'order by id DESC LIMIT 20 ";
// echo $consulta;
//$resultado=$mi_conexion->mysqli_query($consulta);
$resultado = mysqli_query($conn, $consulta);
?>
    <div  class="ibox-title text-success"><i class="fa fa-file-text"></i> Lecturas Recientes de <?php echo $centro; ?></div>
    <div class="ibox-content">
            <table class="table table-bordered ">


                <tr class="text-info">
                    <th><small> Direccion</small></th>
                    <th ><small> Velocidad (Km/h)</small></th>
                    <th ><small> Velocidad (Nudos)</small></th>
                    <th><small> Fecha Hora</small></th>
                </tr>
            <?php
            while ($columna = mysqli_fetch_array( $resultado ))

            {
                $direccion2=dime_tu_direccion( $columna['direccion']);
                $nudos_promedio=($columna['velocidad']/1.85);
                $nudos_promedio=number_format($nudos_promedio,2);
                $velocidad=number_format($columna['velocidad'],2);
                echo "<tr>";
                echo "<td><small>" . $direccion2 . "</td><td>" .$velocidad . "</small></td><td>".$nudos_promedio."</td><td><small>" . $columna['fecha_hora'] . "</small></td>";
                echo "</tr>";
            }

            ?>
            </table>
    </div>
<?php

function dime_tu_direccion($direccion){
    if (($direccion>=340)||($direccion<=20))
        $dir="NORTE";
    else if (($direccion>20)&&($direccion<65))
        $dir="NORESTE";
    else if (($direccion>=65)&&($direccion<115))
        $dir="ESTE";
    else if (($direccion>=115)&&($direccion<155))
        $dir="SURESTE";
    else if (($direccion>=155)&&($direccion < 200))
        $dir="SUR";
    else if (($direccion>=200)&&($direccion < 245))
        $dir="SUROESTE";
    else if (($direccion>=245)&&($direccion < 295))
        $dir="OESTE";
    else
        $dir="NOROESTE ";
    return $dir;

}
?>