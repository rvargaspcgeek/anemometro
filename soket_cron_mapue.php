<?php
date_default_timezone_set('America/Santiago');
require_once ('conexion.php');

for ($i=0;$i<=17;$i++){
    $hoy = date('Y-m-d H:i:s');

    $socket=socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
    if($socket===false){
        throw new Exception("socket_create() failed: reason: " . socket_strerror(socket_last_error()));
    }
    assert(socket_set_block ( $socket)===true);
    assert(socket_bind($socket,0,mt_rand(1024,5000))!==false);//TODO: don't use mt_rand... it has a (small) chance choosing a used address..
    if(socket_connect($socket,
            'aticonsultora.dvrdns.org',8583
        )!==true){
        throw new Exception("socket_connect() failed: reason: " . socket_strerror(socket_last_error($socket)));
    }

    $buffer="";

    socket_recv($socket,$buffer,22,0);
    $direccion=substr($buffer,3,3);
    $velocidad=substr($buffer,7,6);
    $velocidad=$velocidad * 3.6;
    $direccion2=dime_tu_direccion($direccion);
    $direccion3=dime_tu_direccion3($direccion2);



    if (($direccion=="")&&($velocidad==0)){
        echo "Conectando Anemometro ....";
    }
    else{
        echo 'Dirección: '.$direccion2 . ' <small>('.$direccion.' Grados.) </small> <br> Velocidad: '.$velocidad .' Km/h';


        $mi_conexion= new conexion();
        $conn= $mi_conexion->conectar();
        $conn=$mi_conexion->conexion;
        $consulta = "insert into mapue values (NULL ,1,1,'".$velocidad."','".$direccion."','".$direccion3."','".$hoy."')";
        // echo $consulta;
        $resultado = mysqli_query($conn, $consulta);
        //sleep('1');
    }
    socket_close($socket);
    sleep(3);
}

function dime_tu_direccion($direccion){
    if (($direccion>=340)||($direccion<=20))
        $dir="NORTE";
    else if (($direccion>20)&&($direccion<65))
        $dir="NORESTE";
    else if (($direccion>=65)&&($direccion<115))
        $dir="ESTE";
    else if (($direccion>=115)&&($direccion<155))
        $dir="SURESTE";
    else if (($direccion>=155)&&($direccion < 200))
        $dir="SUR";
    else if (($direccion>=200)&&($direccion < 245))
        $dir="SUROESTE";
    else if (($direccion>=245)&&($direccion < 295))
        $dir="OESTE";
    else
        $dir="NOROESTE ";
    return $dir;

}

function dime_tu_direccion3($dir)
{
    switch ($dir) {
        case 'NORTE':
            $dir3 = 0;
            break;
        case 'NORESTE':
            $dir3 = 1;
            break;
        case 'ESTE':
            $dir3 = 2;
            break;
        case 'SURESTE':
            $dir3 = 3;
            break;
        case 'SUR':
            $dir3 = 4;
            break;
        case 'SUROESTE':
            $dir3 = 5;
            break;
        case 'OESTE':
            $dir3 = 6;
            break;
        case 'NOROESTE':
            $dir3 = 7;
            break;

        default:
            $dir3 = "";


    }
    return $dir3;
}