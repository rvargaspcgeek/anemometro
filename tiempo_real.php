<?php
date_default_timezone_set('America/Santiago');
$centro=$_GET["centro"];
?>
<body>
<div class="ibox float-e-margins">
    <div class="ibox-title" id="titulo_div_real">                                                <!--  variable intervalo que viene del index-->
        <span class=" ladda-button btn btn-success btn-xs"  data-style="expand-right" id="botoncito"   onclick="intervalo=setInterval('carga_socket(\'<?php echo $centro; ?>\')',2000)"><i class="fa fa-television"></i> Ver en Tiempo Real</span>


    </div>
    <div class="ibox-content" id="rosa">

    </div>
</div>
</body>
<script>

    function carga_socket(centro) {


        $.ajax({
            url:'soket.php?centro='+centro+'',
            type:'get',

            success: function (response) {
               // console.log('Largo del msg: ' + response.length)
                if (response.length > 46){
                    clearInterval(intervalo);  //detengo la ejecucion del socket en tiempo real  en caso de error
                    console.log('Largo del msg: ' + response.length)
                    console.log('Se detiene la ejecucion en tiempo real')
                }
                var t1 = $("#main").text();
                var t2 = t1 + '<hr>'+ response;
                $("#rosa").html(t2);
                $("#rosa").html(response);
                $("#botoncito").css('visibility','hidden');
                var etiqueta=' <span class="label label-primary" id="etiqueta" >Datos en Tiempo real</span>';
                $("#etiqueta").css('visibility','visible');
                $("#titulo_div_real").html(etiqueta);
                // console.log(t2);
                datos_historicos(centro);

            }
        })

    }

    $(document).ready(function (){

        // Bind normal buttons
        Ladda.bind( '.ladda-button');

        // Bind progress buttons and simulate loading progress
        Ladda.bind( '.progress-demo .ladda-button',{
            callback: function( instance ){
                var progress = 0;
                var interval = setInterval( function(){
                    progress = Math.min( progress + Math.random() * 0.1, 1 );
                    instance.setProgress( progress );

                    if( progress === 1 ){
                        instance.stop();
                        clearInterval( interval );
                    }
                }, 200 );
            }
        });


        var l = $( '.ladda-button-demo' ).ladda();

        l.click(function(){
            // Start loading
            l.ladda( 'start' );

            // Timeout example
            // Do something in backend and then stop ladda
            setTimeout(function(){
                l.ladda('stop');
            },1000)


        });

    });

</script>