<?php
session_start();
if ( $_SESSION["id_perfil"]==""){
    header("Location: login.php");
    die();
    }
date_default_timezone_set('America/Santiago');
require_once ('conexion.php');
$t = new DateTime( date('G:i:s') );
$t->sub( new DateInterval('PT1H') );
$hora_atras=$t->format('Y-m-d H:i:s');
//resource imagegrabscreen ( void );

$mi_conexion= new conexion();
$conn= $mi_conexion->conectar();

//$mi_conexion->conectar();
$conn=$mi_conexion->conexion;
$id_usuario=$_SESSION["id_usuario"];
$consulta="select * from view_accesos where id_usuario=".$id_usuario;
//echo $consulta;
$resultado = mysqli_query($conn, $consulta);

?>



<html lang="es">
<head>
    <title>Anemometro</title>
    <meta http-equiv="Content-type" content="text/html; charset=UTF-8">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet"> <!-- Boton con spinner -->
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

   <!-- esta referencia le da un formato al player-->
    <link rel='stylesheet' id='player-css'  href='https://www.videoexpertsgroup.com/nacl_player_api/vxgplayer-1.8.21.min.css?x45405' type='text/css' media='all' />
    <link rel="shortcut icon" type="image/png" href="img/sol.png"/>


</head>

<body class="ibox-content">


<section>
    <div class="wrapper pageChrome-Plugin">
        <div class="ibox">
            <div class="row  col-lg-12">
                <nav class="navbar navbar-static-top gray-bg col-lg-12" role="navigation" >
                    <div >
                        <div class="col-lg-3">
                            <img src="img/logoinvermar.png" width="250px" height="80px">
                        </div>
                        <div class="col-lg-4">

                            <h2 class="text-info">
                                <select name="sel_centro" id="sel_centro"  style="background-color: #f3f3f4; border: none;">
                                    <option value="">-Seleccione Centro-</option>
                                    <?php
                                    while($columna = mysqli_fetch_array( $resultado )){
                                        echo "<option value=".$columna["id_centro"].">".$columna["nombre_centro"]."</option>";
                                    }
                                    ?>

                                </select>
                                </h2>
                            <ol >
                                <li>
                                    <span >Condición del Viento en tiempo real.</span>
                                </li>
                            </ol>
                        </div>
                        <div class="col-lg-5 pull-left">
                            <ul class="nav navbar-top-links navbar-right">
                                <a href="#" style="padding-right: 10px; " data-toggle="modal" data-target="#myModal5" onclick="captura_pantalle()">
                                    <i class="fa fa-file-image-o"></i>
                                </a>
                                <a href="index.php" style="padding-right: 10px;  ">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </nav>
            </div>
            <div class="row">


                <div class="col-sm-7" style="visibility: hidden;" id="laplayera">
                    <img id="video_vlc" src="anemometro/public/camaras/auchac_00001.png" alt="imagen camara" width="600" height="400">

                    <p><text class="text-muted">Producto Diseñado y Desarrollado por Avanze </text></p>
                </div>
                <div class="col-lg-5">
                    <div class="row">
                         <div class="col-lg-12">
                               <div class="row">
                                    <div class="col-lg-6" >
                                       <div id="tendencia"></div>

                                    </div>
                                    <div class="col-lg-6">
                                        <div  id="tiempo_real"></div>

                                    </div>
                               </div>

                               <div class="row">
                                   <div class="col-lg-6" >
                                       <div id="rosa2" class="text-success" style="padding-top: 20px; ">

                                       </div>
                                   </div>
                                    <div class="col-lg-6">
                                        <h3 class="no-margins">
                                            <div id="rosaa" class="text-success" style="padding-top: 10px; ">
                                            </div>
                                        </h3>
                                   </div>
                               </div>
                         </div>
                    </div>
                    <div class="row ">
                        <div id="datos_historicos"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" id="modal_titulo">
                <div class="pull-left"><img src="img/logoinvermar.jpeg" width="150px"></div>
                <h2 class="modal-title text-info"><span id="titulo_pdf">INVERMAR</span></h2>
                <h4 class="font-bold">Condiciones del viento.</h4>
            </div>
            <div class="modal-body">
                <div id="modal_laplayer"></div>
                <div id="modal_tendencia"></div>
                <div id="rv_captura">
                    <img src="" id="tutos" width="640" height="360" > <!-- el src se lo asigno en vxgplayyer-1.8.31.js linea 450 -->
                </div>

            </div>

            <div class="modal-footer">
                <div id="editor"></div>
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                <button type="button" id="cmd" class="btn btn-warning"><i class="fa fa-file-pdf-o"></i> Guardar PDF </button>
            </div>
        </div>
    </div>
</div>

<?php
function dime_tu_direccion($direccion){
    if (($direccion>=340)||($direccion<=20))
        $dir="NORTE";
    else if (($direccion>20)&&($direccion<65))
        $dir="NORESTE";
    else if (($direccion>=65)&&($direccion<115))
        $dir="ESTE";
    else if (($direccion>=115)&&($direccion<155))
        $dir="SURESTE";
    else if (($direccion>=155)&&($direccion < 200))
        $dir="SUR";
    else if (($direccion>=200)&&($direccion < 245))
        $dir="SUROESTE";
    else if (($direccion>=245)&&($direccion < 295))
        $dir="OESTE";
    else
        $dir="NOROESTE ";
    return $dir;

}
function dime_tu_direccion2($direccion){
    if ($direccion==0)
        $dir="NORTE";
    else if ($direccion==1)
        $dir="NORESTE";
    else if ($direccion==2)
        $dir="ESTE";
    else if ($direccion==3)
        $dir="SURESTE";
    else if ($direccion==4)
        $dir="SUR";
    else if ($direccion==5)
        $dir="SUROESTE";
    else if ($direccion==6)
        $dir="OESTE";
    else if ($direccion==7)
        $dir="NOROESTE ";
    else
        $dir= "No hay lecturas de viento";
    return $dir;

}

?>

<!--<script type='text/javascript' defer="defer" src='https://www.videoexpertsgroup.com/wp-includes/js/wp-embed.min.js?x45405'></script>-->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/vxgplayer-1.8.31.js"></script>
<!-- Ladda botones con spinners   -->
<script src="js/plugins/ladda/spin.min.js"></script>
<script src="js/plugins/ladda/ladda.min.js"></script>
<script src="js/plugins/ladda/ladda.jquery.min.js"></script>


<!-- para guardar los scrrenshot
<script src="js/FileSaver.min.js"></script>-->
<script src="js/html2canvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>


</body>
</html>

<script>
    var intervalo; //Importante enciende o apaga las lecturas en tiempo real  laplayerr
    var centro_seleccionado; //para el titulo en la modal y el pdf

    function datos_historicos(centro) {

        $.ajax({
            url:'datos_historicos.php?centro='+centro+'',
            type:'get',

            success: function (response) {
                $("#datos_historicos").html(response);

            }
        })

    }

    function tendencia(centro) {

        $.ajax({
            url:'tendencia.php?centro='+centro+'',
            type:'get',

            success: function (response) {

                $("#tendencia").html(response);
                $("#modal_tendencia").html(response);


            }
        })

    }
    function tiempo_real(centro) {

        $.ajax({
            url:'tiempo_real.php?centro='+centro+'',
            type:'get',

            success: function (response) {

                $("#tiempo_real").html(response);


            }
        })

    }


    $("#sel_centro").change(function (event) {

    // alert (event.target.value)
    //alert (this.selected)
    console.log('cambio de centro');
    var dis=this;
    var centro=event.target.value;

        centro_seleccionado='CENTRO '+centro.toUpperCase();
        $('#titulo_pdf').html(centro_seleccionado);
       // console.log('centro'+centro_seleccionado);
        clearInterval(intervalo);  //detengo la ejecucion del socket en tiempo real
        datos_historicos(centro);
        tendencia(centro);
        tiempo_real(centro);


        $.ajax( {

            url: 'consulta_ip.php?centro='+centro+'',
            type: "GET",


            //dataType: 'json',
            success: function (data) {
                console.log('primera: '+url);
                console.log('url: '+data);
                var url=data.replace(/\\/g,""); // reemplazar los back slach
                console.log('  nuevaaa url: '+url);
                var url2=url.replace(/"/g,"");  //reemplazar las comillas
                console.log('  nuevaaa url2: '+url2);
                daleplay(url2);




            },
            error: function (data) {
               //console.log("error" + data)
              // dis.url="rtsp://admin:1451r2d2@aticonsultora.dvrdns.org:6066/cam/realmonitor?channel=1&subtype=0";
               alert("hubo un error en la ejecucion");

            }

        })

    })
    function daleplay(url) {
        console.log('daleplay: '+url);
        $("#laplayera").css('visibility','visible');
         
        $("#video_vlc").attr("src", url);
        $("#tutos").attr("src", url);
        //vxgplayer('vxg_media_player1').stop();
        //vxgplayer('vxg_media_player1').src(url);
        //vxgplayer('vxg_media_player1').play();
        // //vxgplayer('vxg_media_player1').showTimeline(true);
        //clearInterval(intervalo);  //detengo la ejecucion del socket en tiempo real

    }
    function captura_pantalle() {

        vxgplayer('vxg_media_player1').takescreenshot();


    }

    //**********************************************************************************************************//
    //*********************************RV dice: COMENZANDO A DIBUJAR PDF ************************************************//

    var specialElementHandlers = {
            '#editor': function (element, renderer) {
                return true;
            }
        };

    $('#cmd').click(function () {

        var logo_invermar;
        var imgData;
        var doc = new jsPDF();
        var img = new Image(),



            canvas = document.createElement("canvas"),
            ctx = canvas.getContext("2d");

        var logo = new Image(),
            logo_canvas = document.createElement("canvas"),
            logo_ctx = logo_canvas.getContext("2d");

        img.onload = function () {
            ctx.width = 400;
            ctx.height = 225;
            ctx.drawImage(img, 0, 0, ctx.width, ctx.height);
            imgData = canvas.toDataURL('image/jpeg');
            doc.addImage(imgData, 'JPEG', 20, 100, 170, 100); //left, top, widht,heght
        }

        logo.onload = function () {
        logo_ctx.width = 300;
        logo_ctx.height = 150;
        logo_ctx.drawImage(logo, 0, 0, logo_ctx.width, logo_ctx.height);
        logo_invermar = logo_canvas.toDataURL('image/jpeg');
        doc.addImage(logo_invermar, 'JPEG', 5, 5, 50, 10); //left, top, widht,heght

            doc.fromHTML($('#modal_tendencia').html(), 20, 30, {
                'width': 170,
                'elementHandlers': specialElementHandlers
            });
            doc.fromHTML($('#modal_titulo').html(), 80, 10, {
                'width': 170,
                'elementHandlers': specialElementHandlers
            });
            doc.save('viento.pdf')
        }

        img.src =  $('#tutos').attr('src');
        logo.src='img/logoinvermar.jpeg';
        //console.log(logo.src);

    });

</script>
